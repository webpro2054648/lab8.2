import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    return this.productsRepository.save(createProductDto);
  }

  findAll() {
    return this.productsRepository.find({ relations: { type: true }});
  }

  findOne(id: number) {
    return this.productsRepository.findOne({
       where: { id },
       relations: { type: true },
      });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const updateProduct = await this.productsRepository.findOneOrFail({
      where: { id },
    });
    await this.productsRepository.update(id, {
      ...updateProduct,
      ...updateProductDto,
    });
    const result = await this.productsRepository.findOne({
      where: { id },
      relations: { type: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.productsRepository.findOneOrFail({
      where: { id },
    });
    await this.productsRepository.remove(deleteProduct);
    return deleteProduct;
  }
}
