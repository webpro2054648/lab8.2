import { Order } from "src/orders/entities/order.entity"
import { Role } from "src/roles/entities/role.entity"
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToMany, JoinTable, UpdateDateColumn, OneToMany } from "typeorm"


@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    email: string

    @Column()
    password: string

    @Column()
    gender: string

    @CreateDateColumn()
    created: Date

    @UpdateDateColumn()
    updated: Date

    @ManyToMany(() => Role, (role) => role.users)
    @JoinTable()
    roles: Role[]

    @OneToMany(() => Order, (order) => order.user)
    orders: Order[]
}

