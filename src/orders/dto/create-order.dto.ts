export class CreateOrderDto {
    orderItems: {
        productTd: number;
        qty: number;
    }[];
    userId: number;
}
